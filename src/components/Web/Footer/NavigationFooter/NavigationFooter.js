import React from 'react';
import { Row, Col } from 'antd';
import {
  BookOutlined,
  CodeOutlined,
  DatabaseOutlined,
  RightOutlined,
  HddOutlined,
  AppstoreOutlined,
  UserOutlined,
} from '@ant-design/icons';

import './NavigationFooter.scss';

export default function Navigationfooter() {
  return (
    <Row className='navigation-footer'>
      <Col span={24}>
        <h3>Navegación</h3>
      </Col>
      <Col md={12}>
        <RenderListLeft />
      </Col>
      <Col md={12}>
        <RenderListRight />
      </Col>
    </Row>
  );
}

function RenderListLeft() {
  return (
    <ul>
      <li>
        <a href='https://www.youtube.com'>
          <BookOutlined /> Cursos Online
        </a>
      </li>
      <li>
        <a href='https://www.youtube.com'>
          <CodeOutlined /> Desarrollo Web
        </a>
      </li>
      <li>
        <a href='https://www.youtube.com'>
          <DatabaseOutlined /> Base de datos
        </a>
      </li>
      <li>
        <a href='https://www.youtube.com'>
          <RightOutlined /> Política de privacidad
        </a>
      </li>
    </ul>
  );
}
function RenderListRight() {
  return (
    <ul>
      <li>
        <a href='https://www.youtube.com'>
          <HddOutlined /> Sistema / Servidores
        </a>
      </li>
      <li>
        <a href='https://www.youtube.com'>
          <AppstoreOutlined /> CMS
        </a>
      </li>
      <li>
        <a href='https://www.youtube.com'>
          <UserOutlined /> Porfolio
        </a>
      </li>
      <li>
        <a href='https://www.youtube.com'>
          <RightOutlined /> Política de Cookies
        </a>
      </li>
    </ul>
  );
}
