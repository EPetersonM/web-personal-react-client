import React, { useState, useEffect } from 'react';
import { Row, Col, Spin, notification } from 'antd';
import { getCoursesApi } from '../api/course';
import PresentationCourses from '../components/Web/Courses/PresentationCourses';
import CoursesList from '../components/Web/Courses/CoursesList';
import { Helmet } from 'react-helmet';

export default function Courses() {
  const [courses, setCourses] = useState(null);

  useEffect(() => {
    getCoursesApi()
      .then((response) => {
        if (response?.code !== 200) {
          notification['warning']({ message: response.message });
        } else {
          setCourses(response.courses);
        }
      })
      .catch(() => {
        notification['error']({
          message: 'Error del servidor, intentelo más tarde.',
        });
      });
  }, []);

  return (
    <>
      <Helmet>
        <title>Cursos | Agustín Navarro Galdón</title>
        <meta
          name='description'
          content='Cursos | Web sobre programación de Agustín Navarro Galdon'
          data-react-helmet='true'
        />
      </Helmet>
      <Row>
        <Col md={16} offset={4}>
          <PresentationCourses />
          {!courses ? (
            <Spin
              tip='Cargando cursos'
              style={{ textAlign: 'center', width: '100%', padding: '20px' }}
            />
          ) : (
            <CoursesList courses={courses} />
          )}
          <CoursesList />
        </Col>
      </Row>
    </>
  );
}
